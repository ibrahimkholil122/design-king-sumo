var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify-es').default,
    cleancss = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel')

gulp.task('sass', function() {
    return gulp.src('assets/build/styles/*.scss' )
        .pipe(sourcemaps.init())
        .pipe( sass() )
        .pipe( cleancss() )
        .pipe(sourcemaps.write(''))
        .pipe( gulp.dest('assets/dist/css' ) );
} );

gulp.task('js', function() {
    return gulp.src('assets/build/scripts/*.js' )
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe( uglify() )
        .pipe(sourcemaps.write(''))
        .pipe( gulp.dest('assets/dist/js' ) );
} );


gulp.task('watch', function() {
    gulp.watch('assets/build/styles/**/*.scss', gulp.series('sass'));
    gulp.watch('assets/build/scripts/*.js',gulp.series('js'));
});