
(function ($) {
	'use-strict';
	$(document).click(function (e) {
		"contestEmailInput" != e.target.id &&
			$(".rainEffect").hasClass("confetti-rain") &&
			($(".rainEffect").fadeTo("fast", 0),
			setTimeout(function () {
				$(".rainEffect").removeClass("confetti-rain");
			}, 900));
	}),
		$(document).ready(function () {
			$("#contestEmailGroup")
				.mouseenter(function () {
					$(".rainEffect").addClass("confetti-rain"), $(".rainEffect").fadeTo("fast", 1);
				})
				.mouseleave(function () {
					$("#contestEmailInput").is(":focus") ||
						($(".rainEffect").fadeTo("fast", 0),
						setTimeout(function () {
							$(".rainEffect").removeClass("confetti-rain");
						}, 900));
				});
		})
})(jQuery);